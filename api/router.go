package api

import (
	
	"api_gateway/api/handler"
	_ "api_gateway/api/docs"
	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

	r.Use(cors.Middleware(cors.Config{
		Origins:        "*",
		RequestHeaders: "Authorization, Origin, Content-Type",
		Methods:        "POST, GET, PUT, DELETE, OPTION",
	}))

	v1Route := r.Group("/v1")

	v1Route.POST("/category", h.CreateCategory)
	v1Route.GET("/category/:id", h.GetCategory)
	v1Route.GET("/categories", h.GetCategoryList)
	v1Route.PUT("/category/:id", h.UpdateCategory)
	v1Route.DELETE("/category/:id", h.DeleteCategory)

	v1Route.POST("/product", h.CreateProduct)
	v1Route.GET("/product/:id", h.GetProduct)
	v1Route.GET("/products", h.GetProductList)
	v1Route.PUT("/ptoduct/:id", h.UpdateProduct)
	v1Route.DELETE("/product/:id", h.DeleteProduct)

	v1Route.POST("/branch", h.CreateBranch)
	v1Route.GET("/branch/:id", h.GetBranch)
	v1Route.GET("/branch", h.GetBranchList)
	v1Route.PUT("/branch/:id", h.UpdateBranch)
	v1Route.DELETE("/branch/:id", h.DeleteBranch)
	v1Route.GET("/active",h.GetActiveBranchList)

	v1Route.POST("/user", h.CreateUser)
	v1Route.GET("/user/:id", h.GetUser)
	v1Route.GET("/users", h.GetUserList)
	v1Route.PUT("/user/:id", h.UpdateUser)
	v1Route.DELETE("/user/:id", h.DeleteUser)

	v1Route.POST("/client", h.CreateClient)
	v1Route.GET("/client/:id", h.GetClient)
	v1Route.GET("/client", h.GetClientList)
	v1Route.PUT("/client/:id", h.UpdateClient)
	v1Route.DELETE("/client/:id", h.DeleteClient)
	v1Route.PATCH("/client",h.UpdatePasswordClient)

	v1Route.POST("/courier", h.CreateCourier)
	v1Route.GET("/courier/:id", h.GetCourier)
	v1Route.GET("/couriers", h.GetCourierList)
	v1Route.PUT("/courier/:id", h.UpdateCourier)
	v1Route.DELETE("/courier/:id", h.DeleteCourier)

	v1Route.POST("/order", h.CreateOrder)
	v1Route.GET("/order/:id", h.GetOrder)
	v1Route.GET("/orders", h.GetOrderList)
	v1Route.PUT("/order/:id", h.UpdateOrder)
	v1Route.DELETE("/order/:id", h.DeleteOrder)

	v1Route.POST("/order_product", h.CreateOrderProducts)
	v1Route.GET("/order_product/:id", h.GetOrderProduct)
	v1Route.GET("/order_products", h.GetOrderProductList)
	v1Route.PUT("/order_product/:id", h.UpdateOrderProduct)
	v1Route.DELETE("/order_product/:id", h.DeleteOrderProduct)

	v1Route.POST("/alternative_tarif",h.CreateAlternativeTarif)
	v1Route.GET("/alternative_tarif/:id", h.GetAlternativeTarif)
	v1Route.GET("/alternative_tarif", h.GetAlternativeTarifList)
	v1Route.PUT("/alternative_tarif/:id", h.UpdateAlternativeTarif)
	v1Route.DELETE("/alternative_tarif/:id", h.DeleteAlterntiveTarif)

	v1Route.POST("/delivery_tarif", h.CreateDeliveryTarif)
	v1Route.GET("/delivery_tarif/:id", h.GetDeliveryTarif)
	v1Route.GET("/delivery_tarif", h.GetDeliveryTarifList)
	v1Route.PUT("/delivery_tarif/:id", h.UpdateDeliveryTarif)
	v1Route.DELETE("/delivery_tarif/:id", h.DeleteDeliveryTarif)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
