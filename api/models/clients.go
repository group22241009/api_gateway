package models

import (
	"time"
)

type Client struct {
	ID               string  `json:"id"`
	FirstName        string  `json:"first_name"`
	LastName         string  `json:"last_name"`
	Phone            string  `json:"phone"`
	Active           bool    `json:"active"`
	Login            string  `json:"login"`
	Password         string  `json:"password"`
	DateOfBirth      string  `json:"date_of_birth"`
	LastOrderedDate  string  `json:"last_ordered_date"`
	TotalOrdersSum   float64 `json:"total_orders_sum"`
	TotalOrdersCount int     `json:"total_orders_count"`
	DiscountType     string  `json:"discount_type"`
	DiscountAmount   float64 `json:"discount_amount"`
}
type CreateClient struct {
	FirstName        string    `json:"first_name"`
	LastName         string    `json:"last_name"`
	Phone            string    `json:"phone"`
	Active           bool      `json:"active"`
	Login            string    `json:"login"`
	Password         string    `json:"password"`
	DateOfBirth      time.Time `json:"date_of_birth"`
	LastOrderedDate  time.Time `json:"last_ordered_date"`
	TotalOrdersSum   float64   `json:"total_orders_sum"`
	TotalOrdersCount int       `json:"total_orders_count"`
	DiscountType     string    `json:"discount_type"`
	DiscountAmount   float64   `json:"discount_amount"`
}

type ClientResponse struct {
	Clients []Client `json:"clients"`
	Count   int      `json:"count"`
}
