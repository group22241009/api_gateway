package models

type OrderProducts struct {
	ID         string  `json:"id"`
	ProductsID string  `json:"product_id"`
	Quantity   int     `json:"qunatity"`
	Price      float32 `json:"price"`
	OrderID    string  `json:"order_id"`
}
type CreateOrderProducts struct {
	ProductsID string  `json:"product_id"`
	Quantity   int     `json:"qunatity"`
	Price      float32 `json:"price"`
	OrderID    string  `json:"order_id"`
}
type OrderProductsResponse struct {
	OrderProducts []OrderProducts `json:"order_products"`
	Count         int             `json:"count"`
}
