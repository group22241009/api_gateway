package models

type Category struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Active      bool   `json:"active"`
	ParentID    string `json:"parent_id"`
	OrderNumber int32 `json:"order_number"`
}

type CreateCategory struct {
	Title       string `json:"title"`
	Active      bool   `json:"active"`
	OrderNumber int32 `json:"order_number"`
}

type CategoryResponse struct {
	Categories []Category `json:"categories"`
	Count      int        `json:"count"`
}
