package models

type DeliveryTarif struct {
	ID        string  `json:"id"`
	Name      string  `json:"name"`
	Type      string  `json:"type"`
	BasePrice float32 `json:"base_price"`
	
}
type CreateDeliveryTarif struct {
	Name      string  `json:"name"`
	Type      string  `json:"type"`
	BasePrice float32 `json:"base_price"`
	
}

type DeliveryTarifResponse struct {
	DeliveryTarifs []DeliveryTarif `json:"delivery_tarifs"`
	Count          int             `json:"count"`
}

