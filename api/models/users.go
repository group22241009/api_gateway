package models

type User struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Phone     string `json:"phone"`
	Active    bool   `json:"active"`
	Login     string `json:"login"`
	Password  string `json:"password"`
}

type CreateUser struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Phone     string `json:"phone"`
	Active    bool   `json:"active"`
	Login     string `json:"login"`
	Password  string `json:"password"`
}

type UserResponse struct {
	Users []User `json:"users"`
	Count int    `json:"count"`
}
