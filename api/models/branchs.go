package models

import "time"

type Branch struct {
	ID            string    `json:"id"`
	Name          string    `json:"name"`
	Phone         string    `json:"phone"`
	DeliveryTarif string    `json:"delivery_tarif"`
	StartHour     time.Time `json:"start_hour"`
	EndHour       time.Time `json:"end_hour"`
	Address       string    `json:"address"`
	Destination   string    `json:"destination"`
	Active        bool      `json:"active"`
}

type CreateBranch struct {
	Name          string `json:"name"`
	Phone         string `json:"phone"`
	DeliveryTarif string `json:"delivery_tarif"`
	StartHour     string `json:"start_hour"`
	EndHour       string `json:"end_hour"`
	Address       string `json:"address"`
	Destination   string `json:"destination"`
	Active        bool   `json:"active"`
}
type BranchResponse struct {
	Branchs []Branch `json:"branchs"`
	Count   int      `json:"count"`
}
