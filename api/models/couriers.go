package models

type Courier struct {
	ID            string `json:"id"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Active        bool   `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int    `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
}
type CreateCourier struct {
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Active        bool   `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int    `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
}
type CourierResponse struct {
	Couriers []Courier `json:"courier"`
	Count    int       `json:"count"`
}
