package models

type Product struct {
	ID          string  `json:"id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	OrderNumber int     `json:"order_number"`
	Active      bool    `json:"active"`
	Type        string  `json:"type"`
	Price       float32 `json:"price"`
}
type CreateProduct struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	OrderNumber int     `json:"order_number"`
	Active      bool    `json:"active"`
	Type        string  `json:"type"`
	Price       float32 `json:"price"`
}
type ProductResponse struct {
	Products []Product `json:"products"`
	Count    int       `json:"count"`
}