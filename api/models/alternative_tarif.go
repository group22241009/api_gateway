package models

type AlternativeTarif struct {
	ID              string  `json:"id"`
	FromPrice       float32 `json:"from_price"`
	ToPrice         float32 `json:"to_price"`
	Price           float32 `json:"price"`

}
type CreateAlternativeTarif struct {
	FromPrice       float32 `json:"from_price"`
	ToPrice         float32 `json:"to_price"`
	Price           float32 `json:"price"`
}
type AlternativeTarifResponse struct {
	AlternativeTarifs []AlternativeTarif `json:"alternative_tarifs"`
	Count             int                `json:"count"`
}
