package models

type Order struct {
	ID            string  `json:"id"`
	ClientID      string  `json:"client_id"`
	BranchID      string  `json:"branch_id"`
	Type          string  `json:"type"`
	Address       string  `json:"address"`
	CourierID     string  `json:"courier_id"`
	Price         float32 `json:"price"`
	DeliveryPrice float32 `json:"delivery_price"`
	Discount      float32 `json:"discount"`
	Status        string  `json:"status"`
	PaymentType   string  `json:"payment_type"`
}
type CreateOrder struct {
	ClientID      string  `json:"client_id"`
	BranchID      string  `json:"branch_id"`
	Type          string  `json:"type"`
	Address       string  `json:"address"`
	CourierID     string  `json:"courier_id"`
	Price         float32 `json:"price"`
	DeliveryPrice float32 `json:"delivery_price"`
	Discount      float32 `json:"discount"`
	Status        string  `json:"status"`
	PaymentType   string  `json:"payment_type"`
}

type OrderResponse struct {
	Orders []Order `json:"orders"`
	Count  int     `json:"count"`
}

