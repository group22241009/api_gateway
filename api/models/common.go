package models

type GetListRequest struct {
	Limit  int    `json:"limit"`
	Page   int    `json:"page"`
	Search string `json:"search"`
}

type PrimaryKey struct {
	ID string `json:"id"`
}
