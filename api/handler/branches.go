package handler

import (
	pb "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateBranch godoc
// @Router       /v1/branch [POST]
// @Summary      Create a new branch
// @Description  Create a new branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        branch body models.CreateBranch false "branch"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateBranch(c *gin.Context) {
	resp := pb.CreateBranch{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	branch, err := h.services.BranchService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating branch", logger.Error(err))
		handleResponse(c, h.log, "error is while creating branch", 500, err)
		return
	}
	h.log.Info("success branch")
	handleResponse(c, h.log, "success", 201, branch)
}

// GetBranch   godoc
// @Router       /v1/branch/{id} [GET]
// @Summary      Get branch by id
// @Description  get branch by id
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_id"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranch(c *gin.Context) {
	id := c.Param("id")

	branch, err := h.services.BranchService().Get(context.Background(), &pb.PrimaryKeyBranch{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting branch by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, branch)
}

// GetBranchList godoc
// @Router        /v1/branch [GET]
// @Summary      Get branch list
// @Description  get branch list
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param         search query string false "search"
// @Success      200  {object}  models.BranchResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	search := c.Query("search")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	branch, err := h.services.BranchService().GetList(ctx, &pb.GetListRequestBranch{Search: search, Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, branch)

}

// UpdateBranch godoc
// @Router        /v1/branch/{id} [PUT]
// @Summary      Update branch
// @Description  update branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch"
// @Param        branch body models.Branch true "branch_id"
// @Success      200  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateBranch(c *gin.Context) {
	branch := pb.Branch{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&branch); err != nil {
		handleResponse(c, h.log, "error while decoding barnch update", 500, err.Error())
		return
	}

	branch.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.BranchService().Update(ctx, &branch)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r.Id)

}

// DeleteBranch godoc
// @Router       /v1/branch/{id} [DELETE]
// @Summary      Delete branch
// @Description  delete branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteBranch(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.BranchService().Delete(ctx, &pb.PrimaryKeyBranch{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting storage", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}

// GetActiveBranchList godoc
// @Router          /v1/active [GET]
// @Summary        get list branch which are active
// @Description    get list branch which are active
// @Tags           active
// @Accept         json
// @Produces       json
// @Success        200  {object}  models.Branch
// @Failure        400  {object}  models.Response
// @Failure        404  {object}  models.Response
// @Failure        500  {object}  models.Response
func (h Handler) GetActiveBranchList(c *gin.Context)  {
	
    var (
        page, limit int
        err         error
    )

    pagestr:= c.DefaultQuery("page", "1")
    page, err = strconv.Atoi(pagestr)
    if err != nil {
        handleResponse(c, h.log, "invalid page parameter", 400, err)
        return
    }
    limitstr := c.DefaultQuery("limit", "10")
    limit, err = strconv.Atoi(limitstr)
    if err != nil {
        handleResponse(c, h.log, "invalid limit parameter", 400, err.Error())
        return
    }

    b, err := h.services.BranchService().GetList(context.Background(), &pb.GetListRequestBranch{Limit: int32(limit), Page: int32(page)})
    if err != nil {
        handleResponse(c, h.log, "failed to get branches", 500, err)
        return
    }

    currentTime := time.Now()
    currentFormattedTime := currentTime.Format("15:04:05")

    var activeBranches []*pb.Branch
	for _, branch := range b.Branchs {
		if branch.StartHour == "" || branch.EndHour == "" {
			continue
		}
		if branch.StartHour <= currentFormattedTime && currentFormattedTime <= branch.EndHour {
			activeBranches = append(activeBranches, branch)
		}
	}
    handleResponse(c, h.log, "Active branches within the specified time range", 200, activeBranches)
}