package handler

import (
	pbc "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateOrderProducts godoc
// @Router       /v1/order_products [POST]
// @Summary      Create a new order_products
// @Description  Create a new order_products
// @Tags         order_products
// @Accept       json
// @Produce      json
// @Param        order_products body models.CreateOrderProducts false "order_products"
// @Success      201  {object}  models.OrderProducts
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrderProducts(c *gin.Context) {
	resp := pbc.CreateOrderProducts{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	order_products, err := h.services.OrderProductService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating order_products", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", 201, order_products)
}

// GetOrderProduct   godoc
// @Router       /v1/order_product/{id} [GET]
// @Summary      Get order_product by id
// @Description  get order_product by id
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product_id"
// @Success      201  {object}  models.OrderProducts
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProduct(c *gin.Context) {
	id := c.Param("id")

	order_product, err := h.services.OrderProductService().Get(context.Background(), &pbc.OrderProductsPrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting order_product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, order_product)
}

// GetOrderProductList godoc
// @Router        /v1/order_products [GET]
// @Summary      Get order_products list
// @Description  get order_products list
// @Tags         order_products
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.OrderProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProductList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	roomorder, err := h.services.OrderProductService().GetList(ctx, &pbc.OrderProductsRequest{Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, roomorder)

}

// UpdateOrderProduct godoc
// @Router        /v1/order_product/{id} [PUT]
// @Summary      Update order_product
// @Description  update order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "room_order_id"
// @Param        order_product body models.OrderProducts true "alternativetarif_id"
// @Success      200  {object}  models.OrderProducts
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrderProduct(c *gin.Context) {
	orderproduct := pbc.OrderProducts{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&orderproduct); err != nil {
		handleResponse(c, h.log, "error while decoding OrderProduct update", 500, err.Error())
		return
	}

	orderproduct.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.OrderProductService().Update(ctx, &orderproduct)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r)

}

// DeleteOrderProduct godoc
// @Router       /v1/order_product/{id} [DELETE]
// @Summary      Delete order_product
// @Description  delete order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "order_product_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrderProduct(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.OrderProductService().Delete(ctx, &pbc.OrderProductsPrimaryKey{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting storage", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}
