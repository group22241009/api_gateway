package handler

import (
	pbc "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateAlternativeTarif godoc
// @Router       /v1/alternative_tarif [POST]
// @Summary      Create a new alternativeTarif
// @Description  Create a new alternativeTarif
// @Tags         alternative_tarif
// @Accept       json
// @Produce      json
// @Param        alternativeTarif body models.CreateAlternativeTarif false "alternative_tarif"
// @Success      201  {object}  models.AlternativeTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateAlternativeTarif(c *gin.Context) {
	resp := pbc.CreateAlternativeTarif{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	alternativeTarif, err := h.services.AlternativeTarifService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating alternativeTarif", logger.Error(err))
		handleResponse(c, h.log, "error creating alternative", 500, err)
		return
	}

	handleResponse(c, h.log, "success", 201, alternativeTarif.Id)
}

// GetAlternativeTarif   godoc
// @Router       /v1/alternative_tarif/{id} [GET]
// @Summary      Get alternative_tarif by id
// @Description  get alternative_tarif by id
// @Tags         alternative_tarif
// @Accept       json
// @Produce      json
// @Param        id path string true "alternative_tarif_id"
// @Success      201  {object}  models.AlternativeTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAlternativeTarif(c *gin.Context) {
	id := c.Param("id")

	alternative_tarif, err := h.services.AlternativeTarifService().Get(context.Background(), &pbc.AlternativeTarifPrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting alternative_tarif by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, alternative_tarif)
}

// GetAlternativeTarifList godoc
// @Router        /v1/alternative_tarif [GET]
// @Summary      Get alternative_tarif list
// @Description  get alternative_tarif list
// @Tags         alternative_tarif
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.AlternativeTarifResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAlternativeTarifList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	AlterntiveTarif, err := h.services.AlternativeTarifService().GetList(ctx, &pbc.AlternativeTRequest{Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, AlterntiveTarif)

}

// UpdateAlternativeTarif godoc
// @Router        /v1/alternative_tarif/{id} [PUT]
// @Summary      Update alternative_tarif
// @Description  update alternative_tarif
// @Tags         alternative_tarif
// @Accept       json
// @Produce      json
// @Param 		 id path string true "alternative_tarif_id"
// @Param        alternative_tarif body models.AlternativeTarif true "alternativetarif_id"
// @Success      200  {object}  models.AlternativeTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateAlternativeTarif(c *gin.Context) {
	alternativeT := pbc.AlternativeTarif{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&alternativeT); err != nil {
		handleResponse(c, h.log, "error while decoding AlterntiveTarif update", 500, err.Error())
		return
	}

	alternativeT.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.AlternativeTarifService().Update(ctx, &alternativeT)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r)

}

// DeleteAlterntiveTarif godoc
// @Router       /v1/alternative_tarif/{id} [DELETE]
// @Summary      Delete alternative_tarif
// @Description  delete alternative_tarif
// @Tags         alternative_tarif
// @Accept       json
// @Produce      json
// @Param 		 id path string true "alternative_tarif_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteAlterntiveTarif(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.AlternativeTarifService().Delete(ctx, &pbc.AlternativeTarifPrimaryKey{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting storage", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}
