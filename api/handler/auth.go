package handler

import (
	"api_gateway/api/models"
	pb "api_gateway/genproto/user_service"
	"api_gateway/pkg/security"
	"context"
	"fmt"

	"github.com/gin-gonic/gin"
)

// UpdatePasswordClient godoc
// @Router       /v1/client/ [PATCH]
// @Summary      Update password
// @Description  Update password
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param         login body models.LoginResponse true "login"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdatePasswordClient(c *gin.Context) {

	login := models.LoginResponse{}
	if err := c.ShouldBindJSON(&login); err != nil {
		fmt.Println("err", err)
	}

	loginrequest := &pb.ClientLoginRequest{
		Login:    login.Login,
		Password: login.OldPassword,

	}
	fmt.Println("**************")
	client, err := h.services.ClientService().GetByCredentials(context.Background(), loginrequest)
	if err != nil {
		fmt.Println("err", err)
	}

	fmt.Println("pass", login.OldPassword)
	if !security.CompareHashAndPassword( client.Password, login.OldPassword) {
		fmt.Println("+++")
		handleResponse(c, h.log, "old password is incorrect", 400, err)
		return
	}

	fmt.Println("******0********")
	response := &pb.UpdatePasswordClient{
		Login:    login.Login,
		Password: login.NewPassword,
	}
	if _, err := h.services.ClientService().Patch(context.Background(), response); err != nil {

		fmt.Println("*****88*********")
		handleResponse(c, h.log, "failed to update password", 500, err)
	}

	handleResponse(c, h.log, "successfully updated new password", 200, "updated")

}
