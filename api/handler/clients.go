package handler

import (
	pb "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateClient godoc
// @Router       /v1/client [POST]
// @Summary      Create a new client
// @Description  Create a new client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        client body models.CreateClient false "client"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateClient(c *gin.Context) {
	resp := pb.CreateClient{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	client, err := h.services.ClientService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating client", logger.Error(err))
		handleResponse(c, h.log, "create client", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 201, client.Id)
}

// GetClient     godoc
// @Router       /v1/client/{id} [GET]
// @Summary      Get client by id
// @Description  get client by id
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "client_id"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClient(c *gin.Context) {
	id := c.Param("id")

	client, err := h.services.ClientService().Get(context.Background(), &pb.PrimaryKeyClient{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting client by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, client)
}

// GetClientList godoc
// @Router        /v1/client [GET]
// @Summary      Get client list
// @Description  get client list
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param         search query string false "search"
// @Success      200  {object}  models.ClientResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClientList(c *gin.Context) {
	var (
		page, limit int
		err         error
		search      string
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	search = c.Query("search")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	client, err := h.services.ClientService().GetList(ctx, &pb.GetListRequestClient{Search: search, Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, client)

}

// UpdateClient godoc
// @Router        /v1/client/{id} [PUT]
// @Summary      Update client
// @Description  update client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param 		 id path string true "client_id"
// @Param        client body models.Client true "client_id"
// @Success      200  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateClient(c *gin.Context) {
	client := pb.Client{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&client); err != nil {
		handleResponse(c, h.log, "error while decoding client update", 500, err.Error())
		return
	}

	client.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.ClientService().Update(ctx, &client)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r.Id)

}

// DeleteClient godoc
// @Router       /v1/client/{id} [DELETE]
// @Summary      Delete client
// @Description  delete client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param 		 id path string true "client_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteClient(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.ClientService().Delete(ctx, &pb.PrimaryKeyClient{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting client", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}
