package handler

import (
	pb "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateUser godoc
// @Router       /v1/user [POST]
// @Summary      Create a new user
// @Description  Create a new user
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        user body models.CreateUser false "user"
// @Success      201  {object}  models.User
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateUser(c *gin.Context) {
	resp := pb.CreateUser{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	user, err := h.services.UserService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating user", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", 201, user)
}

// GetUser   godoc
// @Router       /v1/user/{id} [GET]
// @Summary      Get user by id
// @Description  get user by id
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        id path string true "user_id"
// @Success      201  {object}  models.User
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetUser(c *gin.Context) {
	id := c.Param("id")

	user, err := h.services.UserService().Get(context.Background(), &pb.PrimaryKeyUser{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting user by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, user)
}

// GetUserList godoc
// @Router        /v1/users [GET]
// @Summary      Get users list
// @Description  get users list
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.UserResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetUserList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	user, err := h.services.UserService().GetList(ctx, &pb.GetListRequestUser{Limit:int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, user)

}



// UpdateUser godoc
// @Router        /v1/user/{id} [PUT]
// @Summary      Update user
// @Description  update user
// @Tags         user
// @Accept       json
// @Produce      json
// @Param 		 id path string true "user_id"
// @Param        user body models.User true "user_id"
// @Success      200  {object}  models.User
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateUser(c *gin.Context) {
	user := pb.User{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&user); err != nil {
		handleResponse(c, h.log, "error while decoding User update", 500, err.Error())
		return
	}

	user.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.UserService().Update(ctx, &user)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r)

}

// DeleteUser godoc
// @Router       /v1/user/{id} [DELETE]
// @Summary      Delete user
// @Description  delete user
// @Tags         user
// @Accept       json
// @Produce      json
// @Param 		 id path string true "user_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteUser(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.UserService().Delete(ctx, &pb.PrimaryKeyUser{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting ", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}
