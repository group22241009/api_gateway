package handler

import (
	pb "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateDeliveryTarif godoc
// @Router       /v1/delivery_tarif [POST]
// @Summary      Create a new deliverytarif
// @Description  Create a new deliverytarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        deliverytarif body models.CreateDeliveryTarif false "deliverytarif"
// @Success      201  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateDeliveryTarif(c *gin.Context) {
	resp := pb.CreateDeliveryTarif{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	deliverytarif, err := h.services.DeliveryTarifService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating deliverytarif", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", 201, deliverytarif.Id)
}

// GetDeliveryTarif   godoc
// @Router       /v1/delivery_tarif/{id} [GET]
// @Summary      Get delivery_tarif by id
// @Description  get delivery_tarif by id
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tarif_id"
// @Success      201  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarif(c *gin.Context) {
	id := c.Param("id")

	delivery_tarif, err := h.services.DeliveryTarifService().Get(context.Background(), &pb.PrimaryKeyDelivery{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting delivery_tarif by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, delivery_tarif)
}

// GetDeliveryList godoc
// @Router        /v1/delivery_tarif [GET]
// @Summary      Get delivery_tarif list
// @Description  get delivery_tarif list
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.DeliveryTarifResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarifList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	delivery_tarif, err := h.services.DeliveryTarifService().GetList(ctx, &pb.GetListRequestDelivery{Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, delivery_tarif)

}



// UpdateDeliveryTarif godoc
// @Router        /v1/delivery_tarif/{id} [PUT]
// @Summary      Update delivery_tarif
// @Description  update delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param 		 id path string true "delivery_tarif_if"
// @Param        delivery_tarif body models.AlternativeTarif true "delivery_tarif_id"
// @Success      200  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateDeliveryTarif(c *gin.Context) {
	delivery_tarif := pb.DeliveryTarif{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&delivery_tarif); err != nil {
		handleResponse(c, h.log, "error while decoding DeliveryTarif update", 500, err.Error())
		return
	}

	delivery_tarif.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.DeliveryTarifService().Update(ctx, &delivery_tarif)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r)

}

// DeleteDeliveryTarif godoc
// @Router       /v1/delivery_tarif/{id} [DELETE]
// @Summary      Delete delivery_tarif
// @Description  delete delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param 		 id path string true "delivery_tarif_if"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteDeliveryTarif(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.DeliveryTarifService().Delete(ctx, &pb.PrimaryKeyDelivery{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting ", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}
