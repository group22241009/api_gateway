package handler

import (
	pb "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateCourier godoc
// @Router       /v1/courier [POST]
// @Summary      Create a new courier
// @Description  Create a new courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        courier body models.CreateCourier false "courier"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCourier(c *gin.Context) {
	resp := pb.CreateCourier{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	courier, err := h.services.CourierService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating courier", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", 201, courier)
}

// GetCourier  godoc
// @Router       /v1/courier/{id} [GET]
// @Summary      Get courier by id
// @Description  get courier by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourier(c *gin.Context) {
	id := c.Param("id")

	courier, err := h.services.CourierService().Get(context.Background(), &pb.PrimaryKeyCourier{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting courier by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, courier)
}

// GetCourierList godoc
// @Router        /v1/courier [GET]
// @Summary      Get courier list
// @Description  get courier list
// @Tags         couriers
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.CourierResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourierList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	courier, err := h.services.CourierService().GetList(ctx, &pb.GetListRequestCourier{Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, courier)

}


// UpdateCourier godoc
// @Router        /v1/courier/{id} [PUT]
// @Summary      Update courier
// @Description  update courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param 		 id path string true "courier_id"
// @Param        courier body models.AlternativeTarif true "courier_id"
// @Success      200  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCourier(c *gin.Context) {
	courier := pb.Courier{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&courier); err != nil {
		handleResponse(c, h.log, "error while decoding Courier update", 500, err.Error())
		return
	}

	courier.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.CourierService().Update(ctx, &courier)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r)

}

// DeleteCourier godoc
// @Router       /v1/courier/{id} [DELETE]
// @Summary      Delete courier
// @Description  delete courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param 		 id path string true "courier_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCourier(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.CourierService().Delete(ctx, &pb.PrimaryKeyCourier{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}

