package handler

import (
	pb "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateOrder godoc
// @Router       /v1/order [POST]
// @Summary      Create a new order
// @Description  Create a new order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        order body models.CreateOrder false "order"
// @Success      201  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrder(c *gin.Context) {
	resp := pb.CreateOrder{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	order, err := h.services.OrderService().Create(context.Background(), &resp)
	if err != nil {
		h.log.Error("error is while creating order", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", 201, order)
}

// GetOrder   godoc
// @Router       /v1/order/{id} [GET]
// @Summary      Get order by id
// @Description  get order by id
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order_id"
// @Success      201  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrder(c *gin.Context) {
	id := c.Param("id")

	order, err := h.services.OrderService().Get(context.Background(), &pb.PrimaryKeyOrder{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting order by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, order)
}

// GetOrderList godoc
// @Router        /v1/orders [GET]
// @Summary      Get orders list
// @Description  get orders list
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.OrderResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	roomorder, err := h.services.OrderService().GetList(ctx, &pb.GetListRequestOrder{Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, roomorder)

}

// UpdateOrder godoc
// @Router        /v1/order/{id} [PUT]
// @Summary      Update order
// @Description  update order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param 		 id path string true "order_id"
// @Param        order body models.Order true "order_id"
// @Success      200  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrder(c *gin.Context) {
	order := pb.Order{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&order); err != nil {
		handleResponse(c, h.log, "error ", 500, err.Error())
		return
	}

	order.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.OrderService().Update(ctx, &order)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r)

}

// DeleteOrder godoc
// @Router       /v1/order/{id} [DELETE]
// @Summary      Delete order
// @Description  delete order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param 		 id path string true "order_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrder(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.OrderService().Delete(ctx, &pb.PrimaryKeyOrder{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting ", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}
