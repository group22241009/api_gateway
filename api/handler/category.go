package handler

import (
	pb "api_gateway/genproto/catalog_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateCategory godoc
// @Router       /v1/category [POST]
// @Summary      Create a new category
// @Description  Create a new category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        category body models.CreateCategory false "category"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCategory(c *gin.Context) {
	resp := pb.CreateCategory{}
	if err := c.ShouldBindJSON(&resp); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	category, err := h.services.CategoryService().Create(context.Background(), &resp)
	fmt.Println("resp parent_id",resp.ParentId)
	fmt.Println("resp active",resp.Active)
	fmt.Println("resp order numbver",resp.OrderNumber)
	if err != nil {
		h.log.Error("error is while creating category", logger.Error(err))
		return
	}
	fmt.Println("-------------------------")
	fmt.Println("resp parent_id",resp.ParentId)
	fmt.Println("resp active",resp.Active)
	fmt.Println("resp order numbver",resp.OrderNumber)
	handleResponse(c, h.log, "success", 201, category)
}

// GetCategory   godoc
// @Router       /v1/category/{id} [GET]
// @Summary      Get category by id
// @Description  get category by id
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category_id"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategory(c *gin.Context) {
	id := c.Param("id")

	category, err := h.services.CategoryService().Get(context.Background(), &pb.CategoryPrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting category by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, category)
}

// GetCategoryList godoc
// @Router        /v1/category [GET]
// @Summary      Get category list
// @Description  get category list
// @Tags         categories
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.CategoryResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategoryList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	fmt.Println("***********")
	category, err := h.services.CategoryService().GetList(ctx, &pb.GetListRequestCategory{Limit: int32(limit), Page: int32(page)},)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, category)

}

// UpdateCategory godoc
// @Router        /v1/category/{id} [PUT]
// @Summary      Update category
// @Description  update category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param 		 id path string true "category_id"
// @Param        category body models.Category true "category_id"
// @Success      200  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCategory(c *gin.Context) {
	category := pb.Category{}
	uid := c.Param("id")
	if err := c.ShouldBindJSON(&category); err != nil {
		handleResponse(c, h.log, "error while decoding Category update", 500, err.Error())
		return
	}

	category.Id = uid
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	r, err := h.services.CategoryService().Update(ctx, &category)
	if err != nil {
		handleResponse(c, h.log, "error", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, r)

}

// DeleteCategory godoc
// @Router       /v1/category/{id} [DELETE]
// @Summary      Delete category
// @Description  delete category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param 		 id path string true "category_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCategory(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if _, err := h.services.CategoryService().Delete(ctx, &pb.CategoryPrimaryKey{Id: uid}); err != nil {
		handleResponse(c, h.log, "error while deleting", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, "")

}
