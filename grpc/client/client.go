package client

import (
	"api_gateway/config"
	pbc "api_gateway/genproto/catalog_service"
	pbo "api_gateway/genproto/order_service"
	pbu "api_gateway/genproto/user_service"

	"google.golang.org/grpc"
)

type IServiceManger interface {
	//catalog service
	CategoryService() pbc.CategoryServiceClient
	ProductService() pbc.ProductServiceClient
	//order service
	OrderService() pbo.OrderServiceClient
	OrderProductService() pbo.OrderProductsServiceClient
	AlternativeTarifService() pbo.AlternativeTarifServiceClient
	DeliveryTarifService() pbo.DeliveryTarifServiceClient
	//user service
	UserService() pbu.UserServiceClient
	CourierService() pbu.CourierServiceClient
	ClientService() pbu.ClientServiceClient
	BranchService() pbu.BranchServiceClient
}
type grpcClients struct {
	//catalog service
	categoryService pbc.CategoryServiceClient
	productService  pbc.ProductServiceClient
	//order service
	orderService            pbo.OrderServiceClient
	orderProductService     pbo.OrderProductsServiceClient
	alternativeTarifService pbo.AlternativeTarifServiceClient
	deliveryTarifService    pbo.DeliveryTarifServiceClient
	//user service
	userService    pbu.UserServiceClient
	courierService pbu.CourierServiceClient
	clientService  pbu.ClientServiceClient
	branchService  pbu.BranchServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManger, error) {
	connUserService, err := grpc.Dial(
		cfg.UserGRPCServiceHost+cfg.UserGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connCatalogService, err := grpc.Dial(
		cfg.CatalogGRPCServiceHost+cfg.CatalogGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connOrderService, err := grpc.Dial(
		cfg.OrderGRPCServiceHost+cfg.OrderGRPCServicePort,
		grpc.WithInsecure(),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// user service
		userService:    pbu.NewUserServiceClient(connUserService),
		clientService:  pbu.NewClientServiceClient(connUserService),
		courierService: pbu.NewCourierServiceClient(connUserService),
		branchService:  pbu.NewBranchServiceClient(connUserService),

		// order service
		orderService:            pbo.NewOrderServiceClient(connOrderService),
		orderProductService:     pbo.NewOrderProductsServiceClient(connOrderService),
		alternativeTarifService: pbo.NewAlternativeTarifServiceClient(connOrderService),
		deliveryTarifService:    pbo.NewDeliveryTarifServiceClient(connOrderService),

		//catalog service
		categoryService: pbc.NewCategoryServiceClient(connCatalogService),
		productService:  pbc.NewProductServiceClient(connCatalogService),
	}, nil

}
func (g *grpcClients) CategoryService() pbc.CategoryServiceClient {
	return g.categoryService
}
func (g *grpcClients) ProductService() pbc.ProductServiceClient {
	return g.productService
}
func (g *grpcClients) OrderService() pbo.OrderServiceClient {
	return g.orderService
}
func (g *grpcClients) OrderProductService() pbo.OrderProductsServiceClient {
	return g.orderProductService
}
func (g *grpcClients) AlternativeTarifService() pbo.AlternativeTarifServiceClient {
	return g.alternativeTarifService
}
func (g *grpcClients) DeliveryTarifService() pbo.DeliveryTarifServiceClient {
	return g.deliveryTarifService
}
func (g *grpcClients) UserService() pbu.UserServiceClient {
	return g.userService
}
func (g *grpcClients) CourierService() pbu.CourierServiceClient {
	return g.courierService
}
func (g *grpcClients) ClientService() pbu.ClientServiceClient {
	return g.clientService
}
func (g *grpcClients) BranchService() pbu.BranchServiceClient {
	return g.branchService
}
